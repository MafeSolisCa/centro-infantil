﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Consola.Models
{
    public class Donaciones
    {
        public int IdDonacion { get; set; }
        public string NombreDonador { get; set; }
        public float CantidadDonada { get; set; }
    }
}