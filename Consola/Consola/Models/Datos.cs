﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Consola.Models
{
    public class Datos
    {
        public string Indicador { get; set; }
        public string fechainicio { get; set; }
        public string fechafin { get; set; }
        public string tc { get; set; }
    }
}