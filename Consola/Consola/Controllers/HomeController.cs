﻿using Consola.Helpers;
using Consola.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using BLL;

namespace Consola.Controllers
{
    [SessionManage]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
           

            return View();
        }
        public ActionResult Index2()
        {
            return View();
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [HttpPost]
        public JsonResult ObtenerTipoCambio(String indicador,String fechaini,String fechafin)
        {
            string baseUrl = ConfigurationManager.AppSettings["URL_API"];

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseUrl);
            var contentType = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(contentType);
            JWT jwt = new JWT();
            jwt.Token = HttpContext.Session["token"].ToString();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", jwt.Token);
            Datos tcModel = new Datos();
            tcModel.Indicador = indicador;
            tcModel.fechainicio = fechaini;
            tcModel.fechafin = fechafin;

            string stringData = JsonConvert.SerializeObject(tcModel);
            var contentData = new StringContent(stringData, System.Text.Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.PostAsync("/api/consultas/ObtenerTipoCambio", contentData).Result;
            string stringPersona = response.Content.ReadAsStringAsync().Result;
            List<TipoCambio> data = JsonConvert.DeserializeObject<List<TipoCambio>>(stringPersona);

            if (!response.IsSuccessStatusCode)
            {
                string Message = "Unauthorized!";
                return Json(Message, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }

    }

}