﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using Consola.Models;
using System.Text.RegularExpressions;
using DAL;
using Consola.Tools;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Configuration;
using Newtonsoft.Json;
using System.Net;
using Consola.Helpers;

namespace Consola.Controllers
{
    [SessionManage]
    public class UsuarioController : Controller
    {
        // GET: Usuario
        public ActionResult Index()

        {
            try
            {
                List<Usuario> listaUsuario = new List<Usuario>();
                clsUsuario Usuario = new clsUsuario();
                var data = Usuario.ConsultarUsuario2().ToList();

                foreach (var item in data)
                {
                    if (Session["ROLES"].Equals(1))
                    {
                    Usuario modelo = new Usuario();
                    modelo.IdUsuario = item.IdUsuario;
                    modelo.UserName = item.Usuario;
                    modelo.Password = item.Clave;                  
                    modelo.Estado = item.Estado;                  
                    modelo.Rol = Convert.ToInt32(item.idrol);
                    listaUsuario.Add(modelo);
                    }
                    else
                    {

                        if (Session["US"].Equals(item.Usuario))
                        {
                            Usuario modelo = new Usuario();
                            modelo.IdUsuario = item.IdUsuario;
                            modelo.UserName = item.Usuario;
                            modelo.Password = item.Clave;
                            modelo.Estado = item.Estado;
                            modelo.Rol = Convert.ToInt32(item.idrol);
                            listaUsuario.Add(modelo);
                        }
                    }
                   

                }
                return View(listaUsuario);
            }
            catch
            {
                
                return RedirectToAction("Error", "Error");
            }
        }
       
            // GET: Cliente/Create
            public ActionResult Crear()
            {
            try
            {
                ViewBag.ListaEstado = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
                ViewBag.ListaRol = new SelectList(new[] {
                               new SelectListItem { Value = "1", Text = "Administrador" },
                               new SelectListItem { Value = "2", Text = "Usuario" }
                                                           }, "Value", "Text");

                return View();
            }
            catch (Exception)
            {

                throw;
            }
           
            }
           

        

        // POST: Cliente/Crear
        [HttpPost]
        public ActionResult Crear(Usuario usuario)
        {
            try
            {
                if (ModelState.IsValid)
                {


                    clsUsuario Objcliente = new clsUsuario();
                    bool Resultado = Objcliente.AgregarUsuario(usuario.UserName, Seguridad.Encriptar(usuario.Password), usuario.Estado,usuario.Rol);
                    

                    if (Resultado)
                    {
                        clsBitacora Objbitacora = new clsBitacora();
                        string us = Session["US"].ToString();
                        int rl = Convert.ToInt32(Session["ROLES"].ToString());
                        Objbitacora.AgregarBitacora("UsuarioControler", "Crear", "Registro de usuarios", us, rl);

                        return RedirectToAction("Index");
                    }
                    else
                    {

                        ViewBag.ListaEstado = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
                        ViewBag.ListaRol = new SelectList(new[] {
                               new SelectListItem { Value = "1", Text = "Administrador" },
                               new SelectListItem { Value = "2", Text = "Usuario" }
                                                           }, "Value", "Text");
                        return View("Crear");
                    }
                }
                else
                {
                    ViewBag.ListaEstado = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
                    ViewBag.ListaRol = new SelectList(new[] {
                               new SelectListItem { Value = "1", Text = "Administrador" },
                               new SelectListItem { Value = "2", Text = "Usuario" }
                                                           }, "Value", "Text");

                    return View("Crear");
                }


            }
            catch
            {
                return View();
            }
        }

       // GET: Cliente/Editar/5
        public ActionResult Editar(int id)
        {
            try
            {
                clsUsuario usuario = new clsUsuario();
                ViewBag.ListaEstado = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
                ViewBag.ListaRol = new SelectList(new[] {
                               new SelectListItem { Value = "1", Text = "Administrador" },
                               new SelectListItem { Value = "2", Text = "Usuario" }
                                                           }, "Value", "Text");
                var dato = usuario.ConsultaUsuario(id);

                Usuario modelo = new Usuario();
                modelo.IdUsuario = dato[0].IdUsuario;
                modelo.UserName = dato[0].Usuario;
                modelo.Password = Seguridad.Desencriptar(dato[0].Clave);         
                modelo.Estado = dato[0].Estado;
                modelo.Rol = Convert.ToInt32(dato[0].idrol);
                return View(modelo);
            }
            catch (Exception)
            {

                throw;
            }

        }

        // POST: Cliente/Editar/5
        [HttpPost]
        public ActionResult Editar(int id, Usuario usuario)
        {
            try
            {

                //  if (!Utilerias.ValidarCorreo(cliente.Correo))
                //   {

                //   }
                clsUsuario ObjUsuario = new clsUsuario();
                bool Resultado = ObjUsuario.ActualizarUsuario(usuario.IdUsuario, Seguridad.Encriptar(usuario.Password),usuario.Estado,usuario.Rol);

                if (Resultado)
                {
                    clsBitacora Objbitacora = new clsBitacora();
                    string us = Session["US"].ToString();
                    int rl = Convert.ToInt32(Session["ROLES"].ToString());
                    Objbitacora.AgregarBitacora("UsuarioControler", "Editar", "Editar de usuario", us, rl);
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.ListaEstado = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
                    ViewBag.ListaRol = new SelectList(new[] {
                               new SelectListItem { Value = "1", Text = "Administrador" },
                               new SelectListItem { Value = "2", Text = "Usuario" }
                                                           }, "Value", "Text");


                    return View("Editar");
                }
            }
            catch
            {
                return View();
            }
        }

        // POST: Cliente/Eliminar/5
        public ActionResult Eliminar(int id)
        {
            try
            {
                clsUsuario objcliente = new clsUsuario();
                bool Resultado = objcliente.EliminarUsuario(id);

                if (Resultado)
                {
                    clsBitacora Objbitacora = new clsBitacora();
                    string us = Session["US"].ToString();
                    int rl = Convert.ToInt32(Session["ROLES"].ToString());
                    Objbitacora.AgregarBitacora("ClienteControler", "Eliminar", "Eliminar de usuario", us, rl);
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch
            {
                return View();
            }
        }
    }
}