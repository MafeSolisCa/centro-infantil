﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using Consola.Models;

namespace Consola.Controllers
{
    public class DonacionesController : Controller
    {
        // GET: Donaciones
        public ActionResult Index()
        {
            try
            {
                List<Donaciones> listaDonaciones = new List<Donaciones>();
                clsDonaciones donacion = new clsDonaciones();
                var data = donacion.ConsultarDonaciones().ToList();

                foreach (var item in data)
                {
                    Donaciones modelo = new Donaciones();
                    modelo.IdDonacion = item.IdDonacion;
                    modelo.NombreDonador = item.NombreDonador;
                    modelo.CantidadDonada = Convert.ToInt32(item.CantidadDonada);

                    listaDonaciones.Add(modelo);

                }
                return View(listaDonaciones);
            }
            catch
            {
                string descMsg = "Error consultando la informacion del CLiente.";
                //Bitacora
                return RedirectToAction("Error", "Error");
            }
        }
        public ActionResult Crear()
        {
            try
            {
               
                return View();
            }
            catch (Exception)
            {

                throw;
            }

        }

        // POST: Cliente/Crear
        [HttpPost]
        public ActionResult Crear(Donaciones donaciones)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    clsDonaciones ObjDonaciones = new clsDonaciones();
                    bool Resultado = ObjDonaciones.AgregarDonaciones(donaciones.NombreDonador, donaciones.CantidadDonada);
                    clsBitacora Objbitacora = new clsBitacora();

                    if (Resultado)
                    {
                        string us = Session["US"].ToString();
                        int rl = Convert.ToInt32(Session["ROLES"].ToString());
                        Objbitacora.AgregarBitacora("DonacionesControler", "Crear", "Registro de donaciones", us, rl);

                        return RedirectToAction("Index");
                    }
                    else
                    {
                       
                        return View("Crear");
                    }
                }
                else
                {
                   
                    return View("Crear");
                }


            }
            catch
            {
                return View();
            }
        }
    }
}