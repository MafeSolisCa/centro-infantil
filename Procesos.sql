USE [Proyecto]
GO

/****** Object:  StoredProcedure [dbo].[ActualizarCita]    Script Date: 20/4/2020 23:04:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ActualizarCita]
@IdCita as int,
@Asunto as nvarchar(100),
@Descripcion as nvarchar(200),
@Inicio as Datetime,
@Fin as DateTime,
@ColorFondo as nvarchar(200),
@DiaCompleto as bit

AS
BEGIN
    Update [dbo].[Cita] set
            [Asunto]=@Asunto
           ,[Descripcion]=@Descripcion
           ,[Inicio]=@Inicio
           ,[Fin]=@Fin
           ,[ColorFondo]=@ColorFondo
           ,[DiaCompleto]=@DiaCompleto
		where IdCita=@IdCita
	
END
GO


/****** Object:  StoredProcedure [dbo].[ActualizarCliente]    Script Date: 20/4/2020 23:04:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[ActualizarCliente]
@IdCliente as int,
@IdTipoIdentificacion as int,
@Identificacion as varchar(20),
@Nombre as varchar(50),
@Apellido1 as varchar(50),
@Apellido2 as varchar(50),
@Correo as varchar(50),
@Telefono as varchar(15),
@Provincia as char(1),
@Canton as char(2),
@Distrito as char(2),
@Estado as bit
AS
BEGIN

UPDATE [dbo].[Cliente]
   SET [IdTipoIdentificacion] = @IdTipoIdentificacion
      ,[Identificacion] = @Identificacion
      ,[Nombre] = @Nombre
      ,[Apellido1] = @Apellido1
      ,[Apellido2] = @Apellido2
      ,[Correo] = @Correo
      ,[Telefono] = @Telefono
      ,[Provincia] = @Provincia
      ,[Canton] = @Canton
      ,[Distrito] = @Distrito
      ,[Estado] = @Estado

 WHERE IdCliente=@IdCliente
END

CREATE PROCEDURE [dbo].[ActualizarInstitucion]
@IdInstitucion int,
@Institucion varchar(50),
@IdTipoIdentitifcacion int,
@Identitifcacion varchar(20),
@Telefono varchar(15),
@Direccion varchar(500)=null
AS
BEGIN

	Update [dbo].[Institucion] SET
          IdTipoIdentitifcacion=@IdTipoIdentitifcacion,
		  Identificacion=@Identitifcacion,
		  Telefono=@Telefono,
		  Direccion=@Direccion
		   where IdInstitucion=@IdInstitucion
END

GO

CREATE PROCEDURE [dbo].[ActualizarTipoIdentificacion]
@IdTipoIdentificacion as int,
@TipoIdentificacion as varchar(50),
@Estado as bit
AS
BEGIN



UPDATE [dbo].[TipoIdentificacion]
   SET [TipoIdentificacion] = @TipoIdentificacion
      ,[Estado] = @Estado
 WHERE IdTipoIdentificacion=@IdTipoIdentificacion
END			

GO

/****** Object:  StoredProcedure [dbo].[ActualizarUsuario]    Script Date: 20/4/2020 23:05:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ActualizarUsuario]
@IdUsuario as int,
@Clave as varchar(50),
@Estado as bit,
@idrol as int
AS
BEGIN

    Update usuario SET
          Clave=@Clave,
          Estado=@Estado,
          idrol=@idrol
          where [IdUsuario]=@IdUsuario
END

GO
create PROCEDURE [dbo].[AgregarCalendario]
@IdEvento int,
@Sujeto varchar(100),
@Descripcion varchar(300),
@Comienza datetime,
@Finaliza datetime,
@Color varchar(10),
@TodoElDia bit
AS
BEGIN

Declare @Codigo int

Select @Codigo=isnull(MAX(IdEvento),0)+1 from Calendario

	INSERT INTO Calendario
           (IdEvento
           ,Sujeto
           ,Descripcion
           ,Comienza
           ,Finaliza
           ,Color
		   ,TodoElDia)
     VALUES
           (@IdEvento
           ,@Sujeto
           ,@Descripcion
           ,@Comienza
           ,@Finaliza
           ,@Color
		   ,@TodoElDia)

	Select @Codigo
END

GO
/****** Object:  StoredProcedure [dbo].[AgregarCita]    Script Date: 20/4/2020 23:06:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[AgregarCita]
@Asunto as nvarchar(100),
@Descripcion as nvarchar(200),
@Inicio as Datetime,
@Fin as DateTime,
@ColorFondo as nvarchar(200),
@DiaCompleto as bit

AS
BEGIN
Declare @Codigo int

Select @Codigo=isnull(MAX(IdCita),0)+1 from Cita

    INSERT INTO Cita
           ([IdCita]
		   ,[Asunto]
           ,[Descripcion]
           ,[Inicio]
           ,[Fin]
           ,[ColorFondo]
           ,[DiaCompleto])
     VALUES
           (@Codigo
           ,@Asunto
           ,@Descripcion
           ,@Inicio
           ,@Fin
           ,@ColorFondo
		   ,@DiaCompleto)
	
END
GO

CREATE PROCEDURE [dbo].[AgregarCliente]
@IdTipoIdentificacion as int,
@Identificacion as varchar(20),
@Nombre as varchar(50),
@Apellido1 as varchar(50),
@Apellido2 as varchar(50),
@Correo as varchar(50),
@Telefono as varchar(15),
@Provincia as char(1),
@Canton as char(2),
@Distrito as char(2),
@Estado as bit
AS
BEGIN


Declare @Codigo int

Select @Codigo=isnull(MAX(IdCliente),0)+1 from Cliente

INSERT INTO [dbo].[Cliente]
           ([IdCliente]
           ,[IdTipoIdentificacion]
           ,[Identificacion]
           ,[Nombre]
           ,[Apellido1]
           ,[Apellido2]
           ,[Correo]
           ,[Telefono]
           ,[Provincia]
           ,[Canton]
           ,[Distrito]
           ,[Estado])
     VALUES
           (@Codigo
           ,@IdTipoIdentificacion
           ,@Identificacion
           ,@Nombre
           ,@Apellido1
           ,@Apellido2
           ,@Correo
           ,@Telefono
           ,@Provincia
           ,@Canton
           ,@Distrito
           ,@Estado)
END

GO

CREATE PROCEDURE [dbo].[AgregarInstitucion]
@IdInstitucion int,
@Institucion varchar(50),
@IdTipoIdentitifcacion int,
@Identitifcacion varchar(20),
@Telefono varchar(15),
@Direccion varchar(500)=null
AS
BEGIN

Declare @Codigo int

Select @Codigo=isnull(MAX(IdInstitucion),0)+1 from Institucion

	INSERT INTO [dbo].[Institucion]
           ([IdInstitucion]
           ,[Institucion]
           ,[IdTipoIdentitifcacion]
           ,[Identificacion]
           ,[Telefono]
           ,[Direccion])
     VALUES
           (@Codigo
		   ,@Institucion
           ,@IdTipoIdentitifcacion
		   ,@Identitifcacion
		   ,@Telefono
		   ,@Direccion)

	Select @Codigo
END

GO

CREATE PROCEDURE [dbo].[AgregarPersona]
@Cedula varchar(10),
@Sexo int,
@Nombre varchar(30),
@ApellidoP varchar(26),
@ApellidoM varchar(26),
@Direccion varchar(6)

AS
BEGIN
	
	Declare @Codigo int;

	if exists (SELECT * from Persona where Cedula=@Cedula)
	Begin
		update persona set
			sexo=@Sexo,
			Nombre=@Nombre,
			ApellidoP=@ApellidoP,
			ApellidoM=@ApellidoM,
			Direccion=@Direccion
		where Cedula=@Cedula
	end else
	Begin
		
		Select @Codigo=isnull(MAX(IdPersona),0)+1 from Persona
		
		insert INTO Persona (IdPersona,Cedula,Sexo,Nombre,ApellidoP,ApellidoM,Direccion) 
		values (@Codigo,@Cedula,@Sexo,@Nombre,@ApellidoP,@ApellidoM,@Direccion)
	End
END

GO

CREATE PROCEDURE [dbo].[AgregarTipoIdentificacion]
@TipoIdentificacion as varchar(50),
@Estado as bit
AS
BEGIN

Declare @Codigo int

Select @Codigo=isnull(MAX(IdTipoIdentificacion),0)+1 from TipoIdentificacion

INSERT INTO [dbo].[TipoIdentificacion]
           ([IdTipoIdentificacion]
           ,[TipoIdentificacion]
           ,[Estado])
     VALUES
           (@Codigo
           ,@TipoIdentificacion
           ,@Estado)
END			

GO

/****** Object:  StoredProcedure [dbo].[AgregarUsuario]    Script Date: 20/4/2020 23:08:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AgregarUsuario]
@Usuario as varchar(20),
@Clave as varchar(50),
@Estado as bit,
@idrol as int
AS
BEGIN

Declare @Codigo int

Select @Codigo=isnull(MAX(IdUsuario),0)+1 from Usuario
INSERT INTO Usuario
           (IdUsuario
           ,Usuario
           ,Clave
           ,Estado
           ,idrol)
     VALUES
           (@Codigo
           ,@Usuario
           ,@Clave
           ,@Estado
           ,@idrol)
END

GO

CREATE PROCEDURE [dbo].[Cantones]
@Provincia as char(1)
AS
BEGIN

    -- Insert statements for procedure here
	SELECT * from Canton where Provincia=@Provincia order by Provincia,Canton 
END

GO


create PROCEDURE [dbo].[ConsultaCalendario]
@IdEvento int
AS
BEGIN
	Select * from Calendario where IdEvento=@IdEvento
END

GO

/****** Object:  StoredProcedure [dbo].[ConsultaCita]    Script Date: 20/4/2020 23:09:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ConsultaCita]
@IdCita as int

AS
BEGIN
   Select * from Cita where IdCita=@IdCita
	
END
GO


/****** Object:  StoredProcedure [dbo].[ConsultaCliente]    Script Date: 20/4/2020 23:09:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[ConsultaCliente]
@IdCliente as int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from Cliente where IdCliente=@IdCliente
END

GO

CREATE PROCEDURE [dbo].[ConsultaInstitucion]
@IdInstitucion int
AS
BEGIN
	Select * from Institucion where IdInstitucion=@IdInstitucion
END

GO

CREATE PROCEDURE [dbo].[ConsultaPersona]
@Cedula varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from Persona where Cedula=@Cedula
END

GO


/****** Object:  StoredProcedure [dbo].[ConsultarCita]    Script Date: 20/4/2020 23:10:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ConsultarCita]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * From Cita
END
GO


/****** Object:  StoredProcedure [dbo].[ConsultarCliente]    Script Date: 20/4/2020 23:11:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[ConsultarCliente]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from Cliente
END

GO

/****** Object:  StoredProcedure [dbo].[ConsultarInstitucion]    Script Date: 20/4/2020 23:11:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ConsultarInstitucion]
AS
BEGIN
	Select * from Institucion
END

GO

CREATE PROCEDURE [dbo].[ConsultarTiposIdentificacion]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from TipoIdentificacion
END

GO


CREATE Procedure [dbo].[ConsultarUsuario]

 

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

 

    -- Insert statements for procedure here
    SELECT * from Usuario 
END
GO

CREATE PROCEDURE [dbo].[ConsultaTiposIdentificacion]
@TipoIdentificacion as int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from TipoIdentificacion where TipoIdentificacion=@TipoIdentificacion
END

GO

Create Procedure [dbo].[ConsultaUsuario]
@id int
 

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

 

    -- Insert statements for procedure here
    SELECT * from Usuario where IdUsuario =@id 
END
GO

/****** Object:  StoredProcedure [dbo].[Distritos]    Script Date: 20/4/2020 23:12:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Distritos]
@Provincia as char(1),
@Canton as char(2)
AS
BEGIN

    -- Insert statements for procedure here
	SELECT * from Distrito where Provincia=@Provincia and Canton=@Canton order by Provincia,Canton,Distrito
END

GO


/****** Object:  StoredProcedure [dbo].[EliminaCliente]    Script Date: 20/4/2020 23:13:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[EliminaCliente]
@IdCliente as int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Delete Cliente where IdCliente=@IdCliente
END

GO


/****** Object:  StoredProcedure [dbo].[EliminaInstitucion]    Script Date: 20/4/2020 23:13:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[EliminaInstitucion]
@IdInstitucion int
AS
BEGIN
	Delete Institucion where IdInstitucion=@IdInstitucion
END

GO

/****** Object:  StoredProcedure [dbo].[EliminarCita]    Script Date: 20/4/2020 23:13:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[EliminarCita]
@IdCita as int

AS
BEGIN
   Delete [dbo].[Cita] 
		where IdCita=@IdCita
	
END
GO

/****** Object:  StoredProcedure [dbo].[EliminaTiposIdentificacion]    Script Date: 20/4/2020 23:14:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[EliminaTiposIdentificacion]
@TipoIdentificacion as int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Delete TipoIdentificacion where TipoIdentificacion=@TipoIdentificacion
END

GO

/****** Object:  StoredProcedure [dbo].[EliminaUsuario]    Script Date: 20/4/2020 23:14:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[EliminaUsuario]
@IdUsuario as int
AS
BEGIN
    -- Insert statements for procedure here
    Delete Usuario where IdUsuario=@IdUsuario
END
GO

/****** Object:  StoredProcedure [dbo].[ExisteUsuario]    Script Date: 20/4/2020 23:14:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ExisteUsuario]
@Usuario as varchar(50),
@Clave as varchar(50)
AS
BEGIN

    -- Insert statements for procedure here
	SELECT * from Usuario where Usuario=@Usuario and Clave=@Clave
END

GO


/****** Object:  StoredProcedure [dbo].[Provincias]    Script Date: 20/4/2020 23:15:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Provincias]
AS
BEGIN

    -- Insert statements for procedure here
	SELECT * from Provincia order by Cod
END

GO


/****** Object:  StoredProcedure [dbo].[RegistrarBitacora]    Script Date: 20/4/2020 23:15:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RegistrarBitacora]
@Controlador varchar(50),
@Metodo varchar(50),
@Mensaje varchar(MAX),
@Usuario varchar(50),
@Tipo int
AS
BEGIN

Declare @Codigo int

Select @Codigo=isnull(MAX(IdBitacora),0)+1 from Bitacora

INSERT INTO [dbo].[Bitacora]
           ([IdBitacora]
           ,[Controlador]
           ,[Metodo]
           ,[Mensaje]
           ,[Usuario]
		   ,[Fecha]
		   ,[Tipo])
     VALUES
           (@Codigo
           ,@Controlador
           ,@Metodo
           ,@Mensaje
           ,@Usuario
		   ,GETDATE()
		   ,@Tipo)
END

GO

/****** Object:  StoredProcedure [dbo].[AgregarDonacion]    Script Date: 21/4/2020 18:08:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[AgregarDonacion]
@NombreDonador varchar(50),
@CantidadDonada float
AS
BEGIN
Declare @Codigo int
Select @Codigo=isnull(MAX(IdDonacion),0)+1 from Donaciones
 INSERT INTO Donaciones
           (IdDonacion
		   ,NombreDonador,
		   CantidadDonada)
     VALUES
           (@Codigo
           ,@NombreDonador
		   ,@CantidadDonada)
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultarDonaciones]    Script Date: 21/4/2020 18:09:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[ConsultarDonaciones]
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

 

    -- Insert statements for procedure here
    SELECT * from Donaciones 
END
GO

