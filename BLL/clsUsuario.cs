﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public class clsUsuario
    {
        public List<ExisteUsuarioResult> ConsultarUsuario(string Usuario,string Clave)
        {
            try
            {
                DatosDataContext dc = new DatosDataContext();
                List<ExisteUsuarioResult> data = dc.ExisteUsuario(Usuario,Clave).ToList();
                return data;
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        public List<ConsultaUsuarioResult> ConsultaUsuario(int id)
        {
            try
            {
                DatosDataContext dc = new DatosDataContext();
                List<ConsultaUsuarioResult> data = dc.ConsultaUsuario(id).ToList();
                return data;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public List<ConsultarUsuarioResult> ConsultarUsuario2()
        {
            try
            {
                DatosDataContext dc = new DatosDataContext();
                List<ConsultarUsuarioResult> data = dc.ConsultarUsuario().ToList();
                return data;
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        public bool AgregarUsuario(string usuario, string clave, bool estado, int rol)
        {
            try
            {
                int respuesta = 1;
                DatosDataContext dc = new DatosDataContext();
                respuesta = dc.AgregarUsuario(usuario,clave,estado,rol);

                if (respuesta == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public bool ActualizarUsuario(int IdUsuario,string clave, bool Estado, int rol)
        {
            try
            {
                int respuesta = 1;
                DatosDataContext dc = new DatosDataContext();
                respuesta = dc.ActualizarUsuario(IdUsuario,clave, Estado, rol);
                if (respuesta == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool EliminarUsuario(int IdUsuario)
        {
            try
            {
                DatosDataContext dc = new DatosDataContext();
                dc.EliminaUsuario(IdUsuario);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}
