﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public class clsTipoCambio
    {
        public List<consultatipocambioResult> ConsultaTipoCambio(string Codigo)
        {
            try
            {
                DatosDataContext dc = new DatosDataContext();
                List<consultatipocambioResult> data = dc.consultatipocambio(Codigo).ToList();
                return data;
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        public bool AgregarTipoCambio(string Codigo)
        {
            try
            {
                int respuesta = 1;
                DatosDataContext dc = new DatosDataContext();
                respuesta = Convert.ToInt32(dc.agregartipocambio(Codigo));

                if (respuesta == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
