﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public class clsDonaciones
    {
        public List<ConsultarDonacionesResult> ConsultarDonaciones()
        {
            try
            {
                DatosDataContext dc = new DatosDataContext();
                List<ConsultarDonacionesResult> data = dc.ConsultarDonaciones().ToList();
                return data;
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        public bool AgregarDonaciones(string NombreDonador, float CantidadDonada)
        {
            try
            {
                int respuesta = 1;
                DatosDataContext dc = new DatosDataContext();
                respuesta = dc.AgregarDonacion(NombreDonador, CantidadDonada);

                if (respuesta == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

    }
}
